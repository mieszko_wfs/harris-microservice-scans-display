interface Column {
    title: string;
    id: string;
    widthClass: string;
    template: string;
    data: {
        url: string;
        sseTopic: string;
    };
}

interface Row extends Array<Column> {}
interface Layout extends Array<Row> {}
