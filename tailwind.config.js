/** @type {import('tailwindcss').Config} */
export default {
    content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
    theme: {
        extend: {},
    },
    plugins: [],
    safelist: [
        { pattern: /w-(.*)/ },
        { pattern: /bg-(.*)/ },
        { pattern: /text-(.*)/ },
        { pattern: /rounded(.*)/ },
        { pattern: /p-(.*)/ },
        { pattern: /m-(.*)/ },
    ],
};
